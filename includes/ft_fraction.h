/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fraction.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 23:36:54 by bbellavi          #+#    #+#             */
/*   Updated: 2019/12/09 10:19:09 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#   ifndef FT_FRACTION_H
#   define FT_FRACTION_H

#include <assert.h>
#include <iostream>

class Fraction
{
    int numerator;
    int denominator;
    
    public:
    
    Fraction(int _numerator, int _denominator);
    Fraction(const Fraction &other);
    Fraction(int _numerator);

    int         pgcd() const;
    void        show(std::ostream &stream) const;
    bool        is_reductible() const;
    void        reduce();
    Fraction    operator+=(const Fraction &to_add);
    Fraction    operator*=(const Fraction &to_mul);
    Fraction    operator-=(const Fraction &to_sub);
    Fraction    operator/=(const Fraction &to_div);
};

std::ostream     &operator<<(std::ostream &stream, const Fraction &fraction);

#   endif