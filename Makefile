# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/12/08 23:36:49 by bbellavi          #+#    #+#              #
#    Updated: 2019/12/08 23:50:39 by bbellavi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CPP		= g++

SRC_DIR	= src
INC_DIR	= includes

SRCS	= $(SRC_DIR)/ft_fraction.cpp
SRCS	+= main.cpp
NAME	= ft_fraction

all: $(NAME)

$(NAME): $(SRCS)
	$(CPP) -o $(NAME) $? -I$(INC_DIR)

clean:
	rm -rf $(NAME)

re: clean all