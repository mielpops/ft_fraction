/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 23:37:42 by bbellavi          #+#    #+#             */
/*   Updated: 2019/12/09 10:42:51 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_fraction.h>

#include <iostream>

using namespace std;

int     main(void)
{
    Fraction fraction_1(15, 6);
    Fraction fraction_2(12, 7);

    //cout << fraction_1 << " + " << fraction_2 << " = ";
    fraction_1 += fraction_2;
    //cout << fraction_1 << endl;
    return (0);
}